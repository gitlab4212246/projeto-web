Feature('Login')

Scenario('Login com sucesso', ({ I }) => {
    
    I.amOnPage('https://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 10)
    I.fillField('#user', 'thinaregina@gmail.com')
    I.fillField('#password', '123456')
    I.click('#btnLogin')
    I.waitForText('Login realizado', 3)

}).tag('@sucesso')

Scenario('Tentando Logar digitando apenas o e-mail', ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 10)
    I.fillField('#user', 'thinaregina@gmail.com')
    I.click('#btnLogin')
}).tag('@email')

Scenario('Tentando Logar sem digitar e-mail e senha', ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 10)
    I.click('#btnLogin')
}).tag('@empty')

Scenario('Tentando Logar digitando apenas a senha', ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 10)
    I.fillField('#password', '123456')
    I.click('#btnLogin')
}).tag('@senha')
